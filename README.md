Xamarin.Forms 샘플
===========

이 저장소의 샘플은 Xamarin.Forms의 다양한 측면을 사용하여 iOS, Android 및 UWP (Universal Windows Platform)에 대한 교차 플랫폼 응용 프로그램을 작성하는 방법을 보여줍니다. 개별 샘플을 다운로드하려면 Xamarin.Forms [샘플 갤러리] (https://developer.xamarin.com/samples/xamarin-forms/)를 방문하십시오.

플랫폼을 추가로 지원하려면 다음 포크를 방문하십시오.

  * Tizen : https://github.com/Samsung/xamarin-forms-samples
  * GTK # : https://github.com/jsuarezruiz/xamarin-forms-samples/tree/gtk

특허
-------

[라이센스 파일] (LICENSE) 및 각 샘플에 첨부 된 추가 라이센스 정보를 참조하십시오.

샘플 제출 가이드 라인
=================

이 저장소는 기여와 제안을 환영합니다. 새 샘플을 작성하려면 새 샘플을 저장소에 가져올 수 있도록 직원과 협력해야합니다. 먼저 제안 된 샘플을 요약 한 [GitHub 문제] (https://github.com/xamarin/xamarin-forms-samples/issues/new)를이 저장소에서 불러옵니다. 이 저장소의 MASTER 분기에있는 샘플은 미리보기 또는 시험판 NuGet 패키지에 의존해서는 안됩니다.

Xamarin.Forms [샘플 갤러리] (https://developer.xamarin.com/samples/xamarin-forms/)는이 저장소에서 제공되므로 각 샘플은 다음 요구 사항을 준수해야합니다.

*  스크린 샷  - 각 플랫폼에서 샘플의 스크린 샷이 적어도 하나 이상있는 스크린 샷입니다 (모든 페이지 또는 모든 주요 기능의 스크린 샷이 바람직 함). 이에 대한 예는 [TodoREST] (https://github.com/xamarin/xamarin-forms-samples/tree/master/WebServices/TodoREST/Screenshots)를 참조하십시오.

*  Readme  - 샘플 이름, 설명 및 작성자 속성이있는 README.md 파일입니다. 이에 대한 예는 [TodoREST] (https://github.com/xamarin/xamarin-forms-samples/blob/master/WebServices/TodoREST/README.md)를 참조하십시오.

*  Metadata  - 다음 정보가있는`Metadata.xml` 파일 :

    *  ID  - 샘플의 GUID.

    *  IsFullApplication  - 샘플이 앱 스토어에 제출 될 수있는 전체 앱인지 또는 피쳐 샘플인지 여부를 나타내는 부울 값입니다.

    *  간단한  - 견본이하는 것의 짧은 설명.

    *  수준  - 샘플의 대상 잠재 고객 수준 : 초급, 중급 또는 고급 시작 샘플은 Beginner로, 플랫폼으로 시작하는 사람들을위한 것입니다. 대부분의 샘플은 중급이며 어려운 API에 깊이 들어가는 몇 가지 샘플은 고급이어야합니다.

    *  최소 라이센스 요구 사항  - Starter, Indie, Business 또는 Enterprise : 샘플을 빌드하고 실행하기 위해 사용자가 가져야하는 라이센스를 나타냅니다.

    *  태그  : 앱 관련 태그 목록입니다. 이것들은:

    * 고급
    * 애니메이션
    * 비헤이비어
    * 사용자 정의 렌더러 
    * 데이터 
    * 종속 서비스 
    * 효과 
    * 게임 
    *시작하기
    * 그래픽 
    *항해
    * 스타일 
    * 템플릿 
    * 텍스트 
    *접촉
    * 사용자 인터페이스 
    *웹 서비스
    * Xamarin Live Player 
    * Xamarin.Forms 
    * XAML 

    *  SupportedPlatforms  : 쉼표로 구분 된 지원 플랫폼 목록입니다. 유효한 값은 현재 iOS, Android 및 Windows입니다.

    *  갤러리  : 샘플이 Xamarin.Forms [샘플 갤러리] (https://developer.xamarin.com/samples/xamarin-forms/)에 나타나야 하는지를 나타내는 부울 값입니다.

    `Metadata.xml` 파일의 예제는 [TodoREST] (https://github.com/xamarin/xamarin-forms-samples/blob/master/WebServices/TodoREST/Metadata.xml)를 참조하십시오.
      빌드 가능 솔루션 및 .csproj 파일  - 프로젝트 _must_ 빌드 및 적절한 프로젝트 스캐 폴딩 (솔루션 + .csproj).

이 방법을 사용하면 모든 샘플을 Xamarin.Forms [샘플 갤러리] (https://developer.xamarin.com/samples/xamarin-forms/)와 통합 할 수 있습니다.

질문이 있으시면, [Xamarin Forums] (https://forums.xamarin.com/)에서 물어보십시오.